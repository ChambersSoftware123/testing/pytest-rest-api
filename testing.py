class TrafficLight():
    
    def __init__(self) -> None:
        self.traffic_light = ""

class TrafficIntersection():

    def traffic_intersection():
        north_south = TrafficLight()
        east_west = TrafficLight()
        north_south.traffic_light = "red"
        east_west.traffic_light = "green"
        no_accidents = True

        while no_accidents:
            print(30)
            if north_south.traffic_light == "red":
                east_west.traffic_light = "yellow"
            else:
                north_south.traffic_light = "yellow"
            print("North South : " + north_south.traffic_light)
            print("East West : " + east_west.traffic_light)
            print(5)
            if north_south.traffic_light == "yellow":
                north_south.traffic_light = "red"
                east_west.traffic_light = "green"
            elif east_west.traffic_light == "yellow":
                east_west.traffic_light = "red"
                north_south.traffic_light = "green"
            print("North South RED : " + north_south.traffic_light)
            print("East West RED: " + east_west.traffic_light)
            if east_west.traffic_light == "green" and north_south.traffic_light == "green":
                no_accidents = False
                
TrafficIntersection.traffic_intersection()
